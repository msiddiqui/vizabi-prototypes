(function() {
  'use strict';

  /**
   * Initialize Bar Chart
   * @param id {String}
   * @param options {Object}
   */
  VizabiPrototype.prototype.BarChart = function () {
    var options = this.options;
    var htmlID = this.htmlID;
    var dataset = this.dataset;

    document.getElementById("stateText").value = JSON.stringify(options, null, ' ');

    var x_axisScaleType = 'linear';
    var y_axisScaleType = options.state.marker.axis_y.scaleType;
    var valueYear = options.state.time.value;
    var endTime = options.state.time.end;
    var startTime = options.state.time.start;

    function scaleType(value){
      if(value == 'log')
        return d3.scale.log();
      else if(value == 'linear')
        return d3.scale.linear();
      else if(value == 'ordinal')
        return d3.scale.ordinal();
    }

    d3.select('#trail-layer')
      .classed('vzb-invisible', true);

    d3.chart('BarChart', {
      initialize: function (opts) {
        var chart = this;

        // chart margins to account for labels.
        // we may want to have setters for this.
        // not sure how necessary that is tbh.
        chart.margins = {
          top: 10,
          bottom: 15,
          left: 50,
          right: 0,
          padding: 10
        };

        // default chart ranges
        chart.x = d3.scale.linear();
        chart.y = d3.scale.linear();

        //find the max lex in the dataset
        chart.datasetmax = function()
        {
          var d = dataset[0];
          var maxvalue = 0;
          for(var iter=0; iter< d.length; iter++) {
            for(var iter2=0; iter2 < d[iter].values.length; iter2++)
            {
              maxvalue = d[iter].values[iter2].lex > maxvalue ? d[iter].values[iter2].lex : maxvalue;
            }
          }
          return maxvalue;
        }();

        //find the min lex in the dataset
        chart.datasetmin = function()
        {
          var d = dataset[0];
          var minvalue = d[0].values[0].lex;
          for(var iter=0; iter< d.length; iter++) {
            for(var iter2=0; iter2 < d[iter].values.length; iter2++)
            {
              minvalue = d[iter].values[iter2].lex > minvalue ? minvalue : d[iter].values[iter2].lex;
            }
          }
          return minvalue;
        }();

        chart.base
          .classed('Barchart', true);

        // non data driven areas (as in not to be independatly drawn)
        chart.areas = {};

        // cache for selections that are layer bases.
        chart.layers = {};

        // make sections for labels and main area
        //  _________________
        // |Y|    bars      |
        // | |              |
        // | |              |
        // | |              |
        // | |______________|
        //   |      X       |

        // -- areas
        chart.areas.ylabels = chart.base.append('g')
          .classed('axis', true)
          .attr('width', chart.margins.left)
          .attr('transform',
          'translate(' + (chart.margins.left - 1) + ',' + (chart.margins.top + 1) + ')');

        // -- actual layers
        chart.layers.bars = chart.base.append('g')
          .classed('bars', true)
          .attr('transform',
          'translate(' + (chart.margins.left + 5) +
          ',' + (chart.margins.top + 1) + ')');

        chart.layers.xlabels = chart.base.append('g')
          .classed('axis', true)
          .attr('height', chart.margins.bottom);

        chart.on("change:width", function () {
          chart.x.range([0, chart.w - chart.margins.left]);
          chart.layers.bars.attr('width', chart.w - chart.margins.left);
          chart.layers.xlabels.attr('width', chart.w - chart.margins.left);
        });

        chart.on("change:height", function () {
          var rangeSubtraction = chart.h - chart.margins.bottom - chart.margins.top;
          chart.y.range([rangeSubtraction, 0]);
          chart.areas.ylabels
            .attr('height', rangeSubtraction - 1);
          chart.layers.bars
            .attr('height', rangeSubtraction);
          chart.layers.xlabels
            .attr('transform', 'translate(' + chart.margins.left + ',' +
            (chart.h - chart.margins.bottom + 1) + ')');
        });

        // make actual layers
        chart.layer('bars', chart.layers.bars, {
          // data format:
          // [ { name : x-axis-bar-label, value : N }, ...]
          dataBind: function (data) {

            chart.data = data;

            // how many bars?
            chart.bars = data.length;

            // compute box size
            chart.bar_width = (chart.w - chart.margins.left - ((chart.bars - 1) *
              chart.margins.padding)) / chart.bars;

            // adjust the x domain - the number of bars.
            chart.x.domain([0, chart.bars]);

            // find the y- max in the data.
            chart.datamax = chart.usermax ||
              d3.max(data, function (d) {
                return d.value;
              });

            chart.y.domain([0, chart.datasetmax]).nice();

            // draw yaxis
            var yAxis = d3.svg.axis()
              .scale(chart.y)
              .orient('left')
              .ticks(5)
              .tickFormat(chart._yformat || d3.format('.0%'));

            chart.areas.ylabels
              .call(yAxis);

            return this.selectAll('rect')
              .data(data, function (d) {
                return d.name;
              });
          },
          insert: function () {
            return this.append('rect')
              .classed('bar', true)
              .classed('highlight', true
              //function (d) {
              //  return d.highlight;
              //}
            );
          },

          events: {
            exit: function () {
              this.remove();
            }
          }
        });

        // a layer for the x text labels.
        chart.layer('xlabels', chart.layers.xlabels, {
          dataBind: function (data) {
            // first append a line to the top.
            this.append('line')
              .attr('x1', 0)
              .attr('x2', chart.w - chart.margins.left)
              .attr('y1', 0)
              .attr('y2', 0)
              .style('stroke', '#999')
              .style('stroke-width', '1px')

              .style('shape-rendering', 'crispEdges');

            return this.selectAll('text')
              .data(data, function (d) {
                return d.name;
              });
          },
          insert: function () {
            return this.append('text')
              .classed('label', true)
              .attr('text-anchor', 'middle')
              .attr('x', function (d, i) {
                return chart.x(i) - 0.5 + chart.bar_width / 2;
              })
              .attr('dy', '1em')
              .text(function (d) {
                return d.name;
              });
          },
          events: {
            exit: function () {
              this.remove();
            }
          }
        });

        // on new/update data
        // render the bars.
        var onEnter = function () {
          this.attr('x', function (d, i) {
            return chart.x(i) - 0.5;
          })
            .attr('y', function (d) {
              return chart.h - chart.y(chart.datasetmax - d.value);
            })
            .attr('val', function (d) {
              return d.value;
            })
            .attr('width', chart.bar_width)
            .attr('height', function (d) {
              return chart.y(chart.datasetmax - d.value) - chart.margins.top - chart.margins.bottom;
            })
            .attr("fill", function (d) {
              return d.color;
            });
        };

        chart.layer('bars').on('enter', onEnter);
        chart.layer('bars').on('update', onEnter);
      },

      // return or set the max of the data. otherwise
      // it will use the data max.
      max: function (datamax) {
        if (!arguments.length) {
          return this.usermax;
        }

        this.usermax = datamax;

        if (this.data) this.draw(this.data);

        return this;
      },

      yFormat: function (format) {
        if (!arguments.length) {
          return this._yformat;
        }
        this._yformat = format;
        return this;
      },

      width: function (newWidth) {
        if (!arguments.length) {
          return this.w;
        }
        // save new width
        this.w = newWidth;

        // adjust the x scale range
        this.x = scaleType(x_axisScaleType)
          .range([this.margins.left, this.w - this.margins.right]);

        // adjust the base width
        this.base.attr('width', this.w);

        this.trigger("change:width");
        if (this.data) this.draw(this.data);

        return this;
      },

      height: function (newHeight) {
        if (!arguments.length) {
          return this.h;
        }

        // save new height
        this.h = newHeight;

        // adjust the y scale
        this.y = scaleType(y_axisScaleType)
          .range([this.h - this.margins.top, this.margins.bottom]);

        // adjust the base width
        this.base.attr('height', this.h);

        this.trigger("change:height");
        if (this.data) this.draw(this.data);
        return this;
      },

      dataSet: function (dataSet) {
        this.dataSet = dataSet;
        return this;
      },

      time: function (time) {
        this.time = time;
        return this;
      }

    });

    var barchart = this.currentChart = d3.select(htmlID)
      .append('svg')
      .chart('BarChart', {
        transform: function (data) {
          var di = data.dSetIndex;
          var vi = data.vSetIndex;

          return data.values.map(function (d) {
            var name = d.name;
            d = d.values[vi];
            var color = options.state.marker.color.palette[d.geo];
            return {
              name: name,
              value: d.lex,
              color: color
            };
          });
        }
      })
      .yFormat(d3.format("d"))
      .width(this.screenSize.width)
      .height(this.screenSize.height);

    /** render bar charts and slider **/
    var initialDataIndex = +valueYear - +startTime; //Sets the initial index to correct value based on 'value' option from state.
    barchart.draw({
      values: dataset[0],
      dSetIndex: 0,
      vSetIndex: initialDataIndex
    });

    var lastTime = null;

    this.Slider({
      transform: function (d, dataset) {
        var data;
        var dsc = dataset[0];
        var dscv;

        if (lastTime != Math.round(d)) {
          for (var i = 0; i < dsc.length; i++) {
            dscv = dataset[0][i].values;
            for (var j = 0; j < dscv.length; j++) {
              if (Math.round(d) == dscv[j].time) {
                data = dscv[j];
                this.draw({
                  values: dataset[0],
                  dSetIndex: i,
                  vSetIndex: j
                });
                break;
              }
            }
            if (data) {
              break;
            }
          }
          lastTime = Math.round(d);
        }
      }
    });
  };
})();
